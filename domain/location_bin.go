package domain

import (
	"context"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type LocationBin struct {
	ID         primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	CategoryID primitive.ObjectID `json:"category_id" bson:"category_id,omitempty"`
	Capacity   int64              `json:"capacity" bson:"capacity,omitempty"`
	Name       string             `json:"name" bson:"name,omitempty"`
}

type LocationBinRepository interface {
	Get(ctx context.Context, locationBin *LocationBin) (LocationBin, error)
	Fetch(ctx context.Context) (res []LocationBin, err error)
	Store(ctx context.Context, locationBin *LocationBin) error
	Update(ctx context.Context, locationBin *LocationBin) error
	Delete(ctx context.Context, locationBin *LocationBin) error
}

type LocationBinUsecase interface {
	Get(ctx context.Context, locationBin *LocationBin) (LocationBin, error)
	Fetch(ctx context.Context) (res []LocationBin, err error)
	Store(ctx context.Context, locationBin *LocationBin) error
	Update(ctx context.Context, locationBin *LocationBin, locationBinData *LocationBin) error
	Delete(ctx context.Context, locationBin *LocationBin) error
}
