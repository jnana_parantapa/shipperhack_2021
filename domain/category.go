package domain

import (
	"context"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Category struct {
	ID             primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	CategoryNumber int64              `json:"category_number" bson:"category_number,omitempty"`
	Name           string             `json:"name" bson:"name,omitempty"`
}

type CategoryRepository interface {
	Get(ctx context.Context, category *Category) (Category, error)
	Fetch(ctx context.Context) (res []Category, err error)
	Store(ctx context.Context, category *Category) error
	Update(ctx context.Context, category *Category) error
	Delete(ctx context.Context, category *Category) error
}

type CategoryUsecase interface {
	Get(ctx context.Context, category *Category) (Category, error)
	Fetch(ctx context.Context) (res []Category, err error)
	Store(ctx context.Context, category *Category) error
	Update(ctx context.Context, category *Category, categoryData *Category) error
	Delete(ctx context.Context, category *Category) error
}
