package domain

import (
	"context"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Inbound struct {
	ID                primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	WarehouseCode     string             `json:"warehouse_code" bson:"warehouse_code,omitempty"`
	TenantCode        string             `json:"tenant_code" bson:"tenant_code,omitempty"`
	Sno               int64              `json:"sno" bson:"sno,omitempty"`
	InvoiceNo         string             `json:"invoice_no" bson:"invoice_no,omitempty"`
	PoNo              string             `json:"po_no" bson:"po_no,omitempty"`
	Mcode             string             `json:"mcode" bson:"mcode,omitempty"`
	Description       string             `json:"description" bson:"description,omitempty"`
	Line              int64              `json:"line" bson:"line,omitempty"`
	ShipmentType      string             `json:"shipment_type" bson:"shipment_type,omitempty"`
	Status            string             `json:"status" bson:"status,omitempty"`
	DocRcvdTimestamp  int64              `json:"doc_rcvd_timestamp" bson:"doc_rcvd_timestamp,omitempty"`
	ReceivedTimestamp int64              `json:"received_timestamp" bson:"received_timestamp,omitempty"`
	GrnDate           int64              `json:"grn_date" bson:"grn_date,omitempty"`
	VerifiedDate      int64              `json:"verified_date" bson:"verified_date,omitempty"`
	ReceivedQty       int64              `json:"received_qty" bson:"received_qty,omitempty"`
	ExpectedQty       int64              `json:"expected_qty" bson:"expected_qty,omitempty"`
	GrnQty            int64              `json:"grn_qty" bson:"grn_qty,omitempty"`
	GoodQty           int64              `json:"good_qty" bson:"good_qty,omitempty"`
	DamageQty         int64              `json:"damage_qty" bson:"damage_qty,omitempty"`
	VolumeCbn         float64            `json:"volumecbn" bson:"volumecbn,omitempty"`
	MfgDate           int64              `json:"mfg_date" bson:"mfg_date,omitempty"`
	ExpDate           int64              `json:"exp_date" bson:"exp_date,omitempty"`
	Batch             string             `json:"batch" bson:"batch,omitempty"`
	Serial            string             `json:"serial" bson:"serial,omitempty"`
	LocationBinID     primitive.ObjectID `json:"location_bin_id" bson:"location_bin_id,omitempty"`
	CategoryID        primitive.ObjectID `json:"category_id" bson:"category_id,omitempty"`
	IsAutomated       bool               `json:"is_automated" bson:"is_automated,omitempty"`
	IsDispatch        bool               `json:"is_dispatch" bson:"is_dispatch,omitempty"`
	IsDone            bool               `json:"is_done" bson:"is_done,omitempty"`
}

type LocationBinCounts struct {
	Category         Category           `json:"category" bson:"category,omitempty"`
	LocationBinCount []LocationBinCount `json:"location_bin_counts" bson:"location_bin_counts,omitempty"`
}

type LocationBinCount struct {
	LocationBin LocationBin `json:"location_bin" bson:"location_bin,omitempty"`
	Count       int64       `json:"count" bson:"count,omitempty"`
}

type LocationBinCountResult struct {
	ID    primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Count int64              `json:"count" bson:"count,omitempty"`
}

type InboundRepository interface {
	Get(ctx context.Context, inbound *Inbound) (Inbound, error)
	Fetch(ctx context.Context) (res []Inbound, err error)
	Store(ctx context.Context, inbound *Inbound) error
	Update(ctx context.Context, inbound *Inbound) error
	Delete(ctx context.Context, inbound *Inbound) error
	CountLocationBins(ctx context.Context) ([]LocationBinCountResult, error)
	DeleteIsDispatchTrue(ctx context.Context) error
}

type InboundUsecase interface {
	Get(ctx context.Context, inbound *Inbound) (Inbound, error)
	Fetch(ctx context.Context) (res []Inbound, err error)
	Store(ctx context.Context, inbound *Inbound) error
	Update(ctx context.Context, inbound *Inbound, inboundData *Inbound) error
	Delete(ctx context.Context, inbound *Inbound) error
	CountLocationBins(ctx context.Context, categories []Category, locationBins []LocationBin) ([]LocationBinCounts, error)
	DeleteIsDispatchTrue(ctx context.Context) error
}
