package http

import (
	"net/http"
	// "strconv"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson/primitive"
	validator "gopkg.in/go-playground/validator.v9"

	"jnana_parantapa/shipperhack_2021/domain"
)

type ResponseError struct {
	Message string `json:"message"`
}

type LocationBinHandler struct {
	LocationBinUsecase domain.LocationBinUsecase
}

func NewLocationBinHandler(e *echo.Echo, locationBinUsecase domain.LocationBinUsecase) {
	handler := &LocationBinHandler{
		LocationBinUsecase: locationBinUsecase,
	}

	e.GET("/api/v1/location-bin/:id", handler.GetById)
	e.GET("/api/v1/location-bin", handler.Fetch)
	e.POST("/api/v1/location-bin/store", handler.Store)
	e.PATCH("/api/v1/location-bin/update", handler.Update)
	e.DELETE("/api/v1/location-bin/delete", handler.Delete)
}

func (p *LocationBinHandler) GetById(c echo.Context) (err error) {
	// Create object id from param
	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return p.Get(c, &domain.LocationBin{ID: id})
}

func (i *LocationBinHandler) Get(c echo.Context, productRequest *domain.LocationBin) (err error) {
	ctx := c.Request().Context()

	product, err := i.LocationBinUsecase.Get(ctx, productRequest)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusOK, product)
}

func (i *LocationBinHandler) Fetch(c echo.Context) error {
	ctx := c.Request().Context()

	res, err := i.LocationBinUsecase.Fetch(ctx)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusOK, res)
}

// Store will store the LocationBin by given request body
func (i *LocationBinHandler) Store(c echo.Context) (err error) {
	var locationBin domain.LocationBin

	err = c.Bind(&locationBin)

	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err.Error())
	}

	var ok bool
	if ok, err = isRequestValid(&locationBin); !ok {
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	ctx := c.Request().Context()
	err = i.LocationBinUsecase.Store(ctx, &locationBin)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusCreated, locationBin)
}

// Update will store the LocationBin by given request body
func (i *LocationBinHandler) Update(c echo.Context) (err error) {
	var locationBinData domain.LocationBin

	err = c.Bind(&locationBinData)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	ctx := c.Request().Context()
	locationBin, err := i.LocationBinUsecase.Get(ctx, &domain.LocationBin{ID: locationBinData.ID})
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	err = i.LocationBinUsecase.Update(ctx, &locationBin, &locationBinData)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusCreated, locationBinData)
}

// Delete will remove the LocationBin by given id
func (i *LocationBinHandler) Delete(c echo.Context) (err error) {
	var locationBinData domain.LocationBin

	err = c.Bind(&locationBinData)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	ctx := c.Request().Context()
	locationBin, err := i.LocationBinUsecase.Get(ctx, &locationBinData)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	err = i.LocationBinUsecase.Delete(ctx, &locationBin)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusCreated, locationBin)
}

func isRequestValid(d *domain.LocationBin) (bool, error) {
	validate := validator.New()
	err := validate.Struct(d)
	if err != nil {
		return false, err
	}
	return true, nil
}

func getStatusCode(err error) int {
	if err == nil {
		return http.StatusOK
	}

	logrus.Error(err)
	switch err {
	case domain.ErrInternalServerError:
		return http.StatusInternalServerError
	case domain.ErrNotFound:
		return http.StatusNotFound
	case domain.ErrConflict:
		return http.StatusConflict
	default:
		return http.StatusInternalServerError
	}
}
