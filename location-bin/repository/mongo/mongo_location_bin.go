package mongo

import (
	"context"

	"jnana_parantapa/shipperhack_2021/domain"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type mongoLocationBinRepository struct {
	Collection *mongo.Collection
}

// NewMongoLocationBinRepository will create an implementation of locationBin.Repository
func NewMongoLocationBinRepository(collection *mongo.Collection) domain.LocationBinRepository {
	return &mongoLocationBinRepository{
		Collection: collection,
	}
}

func (m *mongoLocationBinRepository) Get(ctx context.Context, locationBin *domain.LocationBin) (res domain.LocationBin, err error) {
	filter := locationBin

	if err = m.Collection.FindOne(ctx, filter).Decode(&res); err != nil {
		return domain.LocationBin{}, domain.ErrNotFound
	}

	return
}

func (m *mongoLocationBinRepository) Fetch(ctx context.Context) (res []domain.LocationBin, err error) {
	res = make([]domain.LocationBin, 0)

	cursor, err := m.Collection.Find(ctx, bson.M{})

	if err != nil {
		return make([]domain.LocationBin, 0), err
	}
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var locationBin bson.M
		var pd domain.LocationBin

		if err = cursor.Decode(&locationBin); err != nil {
			return make([]domain.LocationBin, 0), err
		}

		bsonBytes, _ := bson.Marshal(locationBin)
		bson.Unmarshal(bsonBytes, &pd)

		res = append(res, pd)
	}

	return
}

func (m *mongoLocationBinRepository) Store(ctx context.Context, locationBin *domain.LocationBin) (err error) {
	res, err := m.Collection.InsertOne(ctx, locationBin)

	locationBin.ID = res.InsertedID.(primitive.ObjectID)

	return
}

func (m *mongoLocationBinRepository) Update(ctx context.Context, locationBin *domain.LocationBin) (err error) {
	_, err = m.Collection.ReplaceOne(ctx, bson.M{"_id": locationBin.ID}, locationBin)

	if err != nil {
		return err
	}

	return
}

func (m *mongoLocationBinRepository) Delete(ctx context.Context, locationBin *domain.LocationBin) (err error) {
	_, err = m.Collection.DeleteOne(
		ctx,
		bson.M{"_id": locationBin.ID},
	)

	if err != nil {
		return err
	}

	return
}
