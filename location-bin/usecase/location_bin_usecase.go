package usecase

import (
	"context"
	"time"

	"jnana_parantapa/shipperhack_2021/domain"
)

type locationBinUsecase struct {
	locationBinRepo domain.LocationBinRepository
	contextTimeout  time.Duration
}

func NewLocationBinUsecase(d domain.LocationBinRepository, timeout time.Duration) domain.LocationBinUsecase {
	return &locationBinUsecase{
		locationBinRepo: d,
		contextTimeout:  timeout,
	}
}

func (l *locationBinUsecase) Get(c context.Context, locationBin *domain.LocationBin) (res domain.LocationBin, err error) {
	ctx, cancel := context.WithTimeout(c, l.contextTimeout)
	defer cancel()

	res, err = l.locationBinRepo.Get(ctx, locationBin)
	if err != nil {
		return domain.LocationBin{}, err
	}

	return
}

func (l *locationBinUsecase) Fetch(c context.Context) (res []domain.LocationBin, err error) {
	ctx, cancel := context.WithTimeout(c, l.contextTimeout)
	defer cancel()

	res, err = l.locationBinRepo.Fetch(ctx)
	if err != nil {
		return make([]domain.LocationBin, 0), err
	}

	return
}

func (l *locationBinUsecase) Store(c context.Context, locationBin *domain.LocationBin) (err error) {
	ctx, cancel := context.WithTimeout(c, l.contextTimeout)
	defer cancel()

	err = l.locationBinRepo.Store(ctx, locationBin)

	return
}

func (l *locationBinUsecase) Update(c context.Context, locationBin *domain.LocationBin, locationBinData *domain.LocationBin) (err error) {
	ctx, cancel := context.WithTimeout(c, l.contextTimeout)
	defer cancel()

	locationBin = locationBinData
	return l.locationBinRepo.Update(ctx, locationBin)
}

func (l *locationBinUsecase) Delete(c context.Context, locationBin *domain.LocationBin) (err error) {
	ctx, cancel := context.WithTimeout(c, l.contextTimeout)
	defer cancel()

	err = l.locationBinRepo.Delete(ctx, locationBin)

	return
}
