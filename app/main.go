package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	_inboundHttpDelivery "jnana_parantapa/shipperhack_2021/inbound/delivery/http"
	_inboundRepo "jnana_parantapa/shipperhack_2021/inbound/repository/mongo"
	_inboundUcase "jnana_parantapa/shipperhack_2021/inbound/usecase"

	_locationBinHttpDelivery "jnana_parantapa/shipperhack_2021/location-bin/delivery/http"
	_locationBinRepo "jnana_parantapa/shipperhack_2021/location-bin/repository/mongo"
	_locationBinUcase "jnana_parantapa/shipperhack_2021/location-bin/usecase"

	_categoryHttpDelivery "jnana_parantapa/shipperhack_2021/category/delivery/http"
	_categoryRepo "jnana_parantapa/shipperhack_2021/category/repository/mongo"
	_categoryUcase "jnana_parantapa/shipperhack_2021/category/usecase"
)

func init() {
	viper.SetConfigFile(`../config.json`)
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	if viper.GetBool(`debug`) {
		log.Println("Service RUN on DEBUG mode")
	}
}

const (
	// Path to the AWS CA file
	caFilePath = "rds-combined-ca-bundle.pem"

	// Timeout operations after N seconds
	connectTimeout  = 5
	queryTimeout    = 30
	username        = "nbwc"
	password        = "nbwckeren123"
	clusterEndpoint = "docdb-2021-04-10-09-09-51.c1yesea4mnsb.us-east-1.docdb.amazonaws.com:27017"

	// Which instances to read from
	readPreference = "secondaryPreferred"

	connectionStringTemplate = "mongodb://%s:%s@%s/put_away?ssl=true&replicaSet=rs0&readpreference=%s&retryWrites=false"
)

func main() {
	connectionURI := fmt.Sprintf(connectionStringTemplate, username, password, clusterEndpoint, readPreference)

	tlsConfig, err := getCustomTLSConfig(caFilePath)
	if err != nil {
		log.Fatalf("Failed getting TLS configuration: %v", err)
	}

	client, err := mongo.NewClient(options.Client().ApplyURI(connectionURI).SetTLSConfig(tlsConfig))
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), connectTimeout*time.Second)
	defer cancel()

	err = client.Connect(ctx)
	if err != nil {
		log.Fatalf("Failed to connect to cluster: %v", err)
	}

	e := echo.New()

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"*"},
		AllowHeaders:     []string{"authorization", "Content-Type"},
		AllowCredentials: true,
		AllowMethods:     []string{echo.OPTIONS, echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))
	timeoutContext := time.Duration(viper.GetInt("context.timeout")) * time.Second

	mongoDatabase := client.Database("put_away")

	// Location Bin
	locationBinCollection := mongoDatabase.Collection("location_bins")
	locationBinRepo := _locationBinRepo.NewMongoLocationBinRepository(locationBinCollection)
	locationBinUcase := _locationBinUcase.NewLocationBinUsecase(locationBinRepo, timeoutContext)
	_locationBinHttpDelivery.NewLocationBinHandler(e, locationBinUcase)

	// Category Category
	categoryCollection := mongoDatabase.Collection("categories")
	categoryRepo := _categoryRepo.NewMongoCategoryRepository(categoryCollection)
	categoryUcase := _categoryUcase.NewCategoryUsecase(categoryRepo, timeoutContext)
	_categoryHttpDelivery.NewCategoryHandler(e, categoryUcase)

	// Inbound
	inboundCollection := mongoDatabase.Collection("inbounds")
	inboundRepo := _inboundRepo.NewMongoInboundRepository(inboundCollection)
	inboundUcase := _inboundUcase.NewInboundUsecase(inboundRepo, timeoutContext)
	_inboundHttpDelivery.NewInboundHandler(e, inboundUcase, categoryUcase, locationBinUcase)

	log.Fatal(e.Start(viper.GetString("server.address")))
}

func getCustomTLSConfig(caFile string) (*tls.Config, error) {
	tlsConfig := new(tls.Config)
	certs, err := ioutil.ReadFile(caFile)

	if err != nil {
		return tlsConfig, err
	}

	tlsConfig.RootCAs = x509.NewCertPool()
	ok := tlsConfig.RootCAs.AppendCertsFromPEM(certs)

	if !ok {
		return tlsConfig, errors.New("Failed parsing pem file")
	}

	return tlsConfig, nil
}
