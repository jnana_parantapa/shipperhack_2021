package usecase

import (
	"context"
	"time"

	"jnana_parantapa/shipperhack_2021/domain"
)

type categoryUsecase struct {
	categoryRepo   domain.CategoryRepository
	contextTimeout time.Duration
}

func NewCategoryUsecase(d domain.CategoryRepository, timeout time.Duration) domain.CategoryUsecase {
	return &categoryUsecase{
		categoryRepo:   d,
		contextTimeout: timeout,
	}
}

func (l *categoryUsecase) Get(c context.Context, category *domain.Category) (res domain.Category, err error) {
	ctx, cancel := context.WithTimeout(c, l.contextTimeout)
	defer cancel()

	res, err = l.categoryRepo.Get(ctx, category)
	if err != nil {
		return domain.Category{}, err
	}

	return
}

func (l *categoryUsecase) Fetch(c context.Context) (res []domain.Category, err error) {
	ctx, cancel := context.WithTimeout(c, l.contextTimeout)
	defer cancel()

	res, err = l.categoryRepo.Fetch(ctx)
	if err != nil {
		return make([]domain.Category, 0), err
	}

	return
}

func (l *categoryUsecase) Store(c context.Context, category *domain.Category) (err error) {
	ctx, cancel := context.WithTimeout(c, l.contextTimeout)
	defer cancel()

	err = l.categoryRepo.Store(ctx, category)

	return
}

func (l *categoryUsecase) Update(c context.Context, category *domain.Category, categoryData *domain.Category) (err error) {
	ctx, cancel := context.WithTimeout(c, l.contextTimeout)
	defer cancel()

	category = categoryData
	return l.categoryRepo.Update(ctx, category)
}

func (l *categoryUsecase) Delete(c context.Context, category *domain.Category) (err error) {
	ctx, cancel := context.WithTimeout(c, l.contextTimeout)
	defer cancel()

	err = l.categoryRepo.Delete(ctx, category)

	return
}
