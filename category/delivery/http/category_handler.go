package http

import (
	"net/http"

	// "strconv"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson/primitive"
	validator "gopkg.in/go-playground/validator.v9"

	"jnana_parantapa/shipperhack_2021/domain"
)

type ResponseError struct {
	Message string `json:"message"`
}

type CategoryHandler struct {
	CategoryUsecase domain.CategoryUsecase
}

func NewCategoryHandler(e *echo.Echo, categoryUsecase domain.CategoryUsecase) {
	handler := &CategoryHandler{
		CategoryUsecase: categoryUsecase,
	}

	e.GET("/api/v1/category/:id", handler.GetById)
	e.GET("/api/v1/category", handler.Fetch)
	e.POST("/api/v1/category/store", handler.Store)
	e.PATCH("/api/v1/category/update", handler.Update)
	e.DELETE("/api/v1/category/delete", handler.Delete)
}

func (ca *CategoryHandler) GetById(c echo.Context) (err error) {
	// Create object id from param
	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return ca.Get(c, &domain.Category{ID: id})
}

func (ca *CategoryHandler) Get(c echo.Context, categoryRequest *domain.Category) (err error) {
	ctx := c.Request().Context()

	category, err := ca.CategoryUsecase.Get(ctx, categoryRequest)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusOK, category)
}

func (ca *CategoryHandler) Fetch(c echo.Context) error {
	ctx := c.Request().Context()

	res, err := ca.CategoryUsecase.Fetch(ctx)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusOK, res)
}

// Store will store the Category by given request body
func (ca *CategoryHandler) Store(c echo.Context) (err error) {
	var category domain.Category

	err = c.Bind(&category)

	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err.Error())
	}

	var ok bool
	if ok, err = isRequestValid(&category); !ok {
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	ctx := c.Request().Context()
	err = ca.CategoryUsecase.Store(ctx, &category)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusCreated, category)
}

// Update will store the Category by given request body
func (ca *CategoryHandler) Update(c echo.Context) (err error) {
	var categoryData domain.Category

	err = c.Bind(&categoryData)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	ctx := c.Request().Context()
	category, err := ca.CategoryUsecase.Get(ctx, &domain.Category{ID: categoryData.ID})
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	err = ca.CategoryUsecase.Update(ctx, &category, &categoryData)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusCreated, categoryData)
}

// Delete will remove the Category by given id
func (ca *CategoryHandler) Delete(c echo.Context) (err error) {
	var categoryData domain.Category

	err = c.Bind(&categoryData)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	ctx := c.Request().Context()
	category, err := ca.CategoryUsecase.Get(ctx, &categoryData)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	err = ca.CategoryUsecase.Delete(ctx, &category)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusCreated, category)
}

func isRequestValid(d *domain.Category) (bool, error) {
	validate := validator.New()
	err := validate.Struct(d)
	if err != nil {
		return false, err
	}
	return true, nil
}

func getStatusCode(err error) int {
	if err == nil {
		return http.StatusOK
	}

	logrus.Error(err)
	switch err {
	case domain.ErrInternalServerError:
		return http.StatusInternalServerError
	case domain.ErrNotFound:
		return http.StatusNotFound
	case domain.ErrConflict:
		return http.StatusConflict
	default:
		return http.StatusInternalServerError
	}
}
