package mongo

import (
	"context"

	"jnana_parantapa/shipperhack_2021/domain"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type mongoCategoryRepository struct {
	Collection *mongo.Collection
}

// NewMongoCategoryRepository will create an implementation of category.Repository
func NewMongoCategoryRepository(collection *mongo.Collection) domain.CategoryRepository {
	return &mongoCategoryRepository{
		Collection: collection,
	}
}

func (m *mongoCategoryRepository) Get(ctx context.Context, category *domain.Category) (res domain.Category, err error) {
	filter := category

	if err = m.Collection.FindOne(ctx, filter).Decode(&res); err != nil {
		return domain.Category{}, domain.ErrNotFound
	}

	return
}

func (m *mongoCategoryRepository) Fetch(ctx context.Context) (res []domain.Category, err error) {
	res = make([]domain.Category, 0)

	cursor, err := m.Collection.Find(ctx, bson.M{})

	if err != nil {
		return make([]domain.Category, 0), err
	}
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var category bson.M
		var pd domain.Category

		if err = cursor.Decode(&category); err != nil {
			return make([]domain.Category, 0), err
		}

		bsonBytes, _ := bson.Marshal(category)
		bson.Unmarshal(bsonBytes, &pd)

		res = append(res, pd)
	}

	return
}

func (m *mongoCategoryRepository) Store(ctx context.Context, category *domain.Category) (err error) {
	res, err := m.Collection.InsertOne(ctx, category)

	category.ID = res.InsertedID.(primitive.ObjectID)

	return
}

func (m *mongoCategoryRepository) Update(ctx context.Context, category *domain.Category) (err error) {
	_, err = m.Collection.ReplaceOne(ctx, bson.M{"_id": category.ID}, category)

	if err != nil {
		return err
	}

	return
}

func (m *mongoCategoryRepository) Delete(ctx context.Context, category *domain.Category) (err error) {
	_, err = m.Collection.DeleteOne(
		ctx,
		bson.M{"_id": category.ID},
	)

	if err != nil {
		return err
	}

	return
}
