package mongo

import (
	"context"

	"jnana_parantapa/shipperhack_2021/domain"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type mongoInboundRepository struct {
	Collection *mongo.Collection
}

// NewMongoInboundRepository will create an implementation of inbound.Repository
func NewMongoInboundRepository(collection *mongo.Collection) domain.InboundRepository {
	return &mongoInboundRepository{
		Collection: collection,
	}
}

func (m *mongoInboundRepository) Get(ctx context.Context, inbound *domain.Inbound) (res domain.Inbound, err error) {
	filter := inbound

	if err = m.Collection.FindOne(ctx, filter).Decode(&res); err != nil {
		return domain.Inbound{}, domain.ErrNotFound
	}

	return
}

func (m *mongoInboundRepository) Fetch(ctx context.Context) (res []domain.Inbound, err error) {
	res = make([]domain.Inbound, 0)

	cursor, err := m.Collection.Find(ctx, bson.M{})

	if err != nil {
		return make([]domain.Inbound, 0), err
	}
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var inbound bson.M
		var pd domain.Inbound

		if err = cursor.Decode(&inbound); err != nil {
			return make([]domain.Inbound, 0), err
		}

		bsonBytes, _ := bson.Marshal(inbound)
		bson.Unmarshal(bsonBytes, &pd)

		res = append(res, pd)
	}

	return
}

func (m *mongoInboundRepository) Store(ctx context.Context, inbound *domain.Inbound) (err error) {
	res, err := m.Collection.InsertOne(ctx, inbound)

	inbound.ID = res.InsertedID.(primitive.ObjectID)

	return
}

func (m *mongoInboundRepository) Update(ctx context.Context, inbound *domain.Inbound) (err error) {
	_, err = m.Collection.ReplaceOne(ctx, bson.M{"_id": inbound.ID}, inbound)

	if err != nil {
		return err
	}

	return
}

func (m *mongoInboundRepository) Delete(ctx context.Context, inbound *domain.Inbound) (err error) {
	_, err = m.Collection.DeleteOne(
		ctx,
		bson.M{"_id": inbound.ID},
	)

	if err != nil {
		return err
	}

	return
}

func (m *mongoInboundRepository) DeleteIsDispatchTrue(ctx context.Context) (err error) {
	_, err = m.Collection.DeleteMany(
		ctx,
		bson.M{"is_dispatch": true},
	)

	if err != nil {
		return err
	}

	return
}

func (m *mongoInboundRepository) CountLocationBins(ctx context.Context) (res []domain.LocationBinCountResult, err error) {
	groupStage := bson.D{{"$group", bson.D{{"_id", "$location_bin_id"}, {"count", bson.D{{"$sum", 1}}}}}}

	cursor, err := m.Collection.Aggregate(ctx, mongo.Pipeline{groupStage})

	if err != nil {
		return make([]domain.LocationBinCountResult, 0), err
	}
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var locationBinCountP bson.M
		var l domain.LocationBinCountResult

		if err = cursor.Decode(&locationBinCountP); err != nil {
			return make([]domain.LocationBinCountResult, 0), err
		}

		bsonBytes, _ := bson.Marshal(locationBinCountP)
		bson.Unmarshal(bsonBytes, &l)

		res = append(res, l)
	}

	return
}
