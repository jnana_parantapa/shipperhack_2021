package usecase

import (
	"context"
	"time"

	"jnana_parantapa/shipperhack_2021/domain"
)

type inboundUsecase struct {
	inboundRepo    domain.InboundRepository
	contextTimeout time.Duration
}

func NewInboundUsecase(d domain.InboundRepository, timeout time.Duration) domain.InboundUsecase {
	return &inboundUsecase{
		inboundRepo:    d,
		contextTimeout: timeout,
	}
}

func (i *inboundUsecase) Get(c context.Context, inbound *domain.Inbound) (res domain.Inbound, err error) {
	ctx, cancel := context.WithTimeout(c, i.contextTimeout)
	defer cancel()

	res, err = i.inboundRepo.Get(ctx, inbound)
	if err != nil {
		return domain.Inbound{}, err
	}

	return
}

func (i *inboundUsecase) Fetch(c context.Context) (res []domain.Inbound, err error) {
	ctx, cancel := context.WithTimeout(c, i.contextTimeout)
	defer cancel()

	res, err = i.inboundRepo.Fetch(ctx)
	if err != nil {
		return make([]domain.Inbound, 0), err
	}

	return
}

func (i *inboundUsecase) Store(c context.Context, inbound *domain.Inbound) (err error) {
	ctx, cancel := context.WithTimeout(c, i.contextTimeout)
	defer cancel()

	err = i.inboundRepo.Store(ctx, inbound)

	return
}

func (i *inboundUsecase) Update(c context.Context, inbound *domain.Inbound, inboundData *domain.Inbound) (err error) {
	ctx, cancel := context.WithTimeout(c, i.contextTimeout)
	defer cancel()

	inbound = inboundData
	return i.inboundRepo.Update(ctx, inbound)
}

func (i *inboundUsecase) Delete(c context.Context, inbound *domain.Inbound) (err error) {
	ctx, cancel := context.WithTimeout(c, i.contextTimeout)
	defer cancel()

	err = i.inboundRepo.Delete(ctx, inbound)

	return
}

func (i *inboundUsecase) DeleteIsDispatchTrue(c context.Context) (err error) {
	ctx, cancel := context.WithTimeout(c, i.contextTimeout)
	defer cancel()

	err = i.inboundRepo.DeleteIsDispatchTrue(ctx)

	return
}

func (i *inboundUsecase) CountLocationBins(c context.Context, categories []domain.Category, locationBins []domain.LocationBin) (res []domain.LocationBinCounts, err error) {
	ctx, cancel := context.WithTimeout(c, i.contextTimeout)
	defer cancel()

	counts, err := i.inboundRepo.CountLocationBins(ctx)

	res = make([]domain.LocationBinCounts, 0)
	// This is bad, I know it
	for _, category := range categories {
		r := domain.LocationBinCounts{}
		r.Category = category
		for _, locationBin := range locationBins {
			for _, count := range counts {
				if count.ID == locationBin.ID {
					if locationBin.CategoryID == category.ID {
						r.LocationBinCount = append(r.LocationBinCount, domain.LocationBinCount{LocationBin: locationBin, Count: count.Count})
					}
				}
			}
		}
		res = append(res, r)
	}

	return
}
