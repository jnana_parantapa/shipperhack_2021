package http

import (
	"net/http"
	// "strconv"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson/primitive"
	validator "gopkg.in/go-playground/validator.v9"

	"jnana_parantapa/shipperhack_2021/domain"
)

type ResponseError struct {
	Message string `json:"message"`
}

type InboundHandler struct {
	InboundUsecase     domain.InboundUsecase
	CategoryUsecase    domain.CategoryUsecase
	LocationBinUsecase domain.LocationBinUsecase
}

func NewInboundHandler(e *echo.Echo, inboundUsecase domain.InboundUsecase, categoryUsecase domain.CategoryUsecase, locationBinUsecase domain.LocationBinUsecase) {
	handler := &InboundHandler{
		InboundUsecase:     inboundUsecase,
		CategoryUsecase:    categoryUsecase,
		LocationBinUsecase: locationBinUsecase,
	}

	e.GET("/api/v1", handler.PlaceHolder)
	e.GET("/api/v1/inbound", handler.Fetch)
	e.POST("/api/v1/inbound/store", handler.Store)
	e.PATCH("/api/v1/inbound/update", handler.Update)
	e.DELETE("/api/v1/inbound/delete", handler.Delete)
	e.DELETE("/api/v1/inbound/delete-is-dispatch", handler.DeleteIsDispatchTrue)
	e.GET("/api/v1/inbound/count-location-bins", handler.CountLocationBins)
}

func (p *InboundHandler) PlaceHolder(c echo.Context) (err error) {
	return c.JSON(http.StatusOK, "ok")
}

func (p *InboundHandler) GetById(c echo.Context) (err error) {
	// Create object id from param
	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return p.Get(c, &domain.Inbound{ID: id})
}

func (i *InboundHandler) Get(c echo.Context, productRequest *domain.Inbound) (err error) {
	ctx := c.Request().Context()

	product, err := i.InboundUsecase.Get(ctx, productRequest)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusOK, product)
}

func (i *InboundHandler) Fetch(c echo.Context) error {
	ctx := c.Request().Context()

	res, err := i.InboundUsecase.Fetch(ctx)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusOK, res)
}

// Store will store the Inbound by given request body
func (i *InboundHandler) Store(c echo.Context) (err error) {
	var inbound domain.Inbound

	err = c.Bind(&inbound)

	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err.Error())
	}

	var ok bool
	if ok, err = isRequestValid(&inbound); !ok {
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	ctx := c.Request().Context()
	err = i.InboundUsecase.Store(ctx, &inbound)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusCreated, inbound)
}

// Update will store the Inbound by given request body
func (i *InboundHandler) Update(c echo.Context) (err error) {
	var inboundData domain.Inbound

	err = c.Bind(&inboundData)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	ctx := c.Request().Context()
	inbound, err := i.InboundUsecase.Get(ctx, &domain.Inbound{ID: inboundData.ID})
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	err = i.InboundUsecase.Update(ctx, &inbound, &inboundData)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusCreated, inboundData)
}

// Delete will remove the Inbound by given id
func (i *InboundHandler) Delete(c echo.Context) (err error) {
	var inboundData domain.Inbound

	err = c.Bind(&inboundData)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	ctx := c.Request().Context()
	inbound, err := i.InboundUsecase.Get(ctx, &inboundData)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	err = i.InboundUsecase.Delete(ctx, &inbound)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusCreated, inbound)
}

func (i *InboundHandler) DeleteIsDispatchTrue(c echo.Context) (err error) {
	ctx := c.Request().Context()

	err = i.InboundUsecase.DeleteIsDispatchTrue(ctx)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusCreated, "ok")
}

func (i *InboundHandler) CountLocationBins(c echo.Context) error {
	ctx := c.Request().Context()

	categories, err := i.CategoryUsecase.Fetch(ctx)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	locationBins, err := i.LocationBinUsecase.Fetch(ctx)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	res, err := i.InboundUsecase.CountLocationBins(ctx, categories, locationBins)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusOK, res)
}

func isRequestValid(d *domain.Inbound) (bool, error) {
	validate := validator.New()
	err := validate.Struct(d)
	if err != nil {
		return false, err
	}
	return true, nil
}

func getStatusCode(err error) int {
	if err == nil {
		return http.StatusOK
	}

	logrus.Error(err)
	switch err {
	case domain.ErrInternalServerError:
		return http.StatusInternalServerError
	case domain.ErrNotFound:
		return http.StatusNotFound
	case domain.ErrConflict:
		return http.StatusConflict
	default:
		return http.StatusInternalServerError
	}
}
